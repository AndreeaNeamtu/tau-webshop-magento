package homepage;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import setup.ScreenShooter;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class FilterPage extends ScreenShooter {

    /**
     * Selectors
     */

    public final ElementsCollection color = $$("#narrow-by-list img");
    public final ElementsCollection colorCount = $$("#narrow-by-list dd li a[href*='color'] .count");
    public final SelenideElement countBelow100Selector = $("#narrow-by-list dd li a[href*='-100'] .count");
    public final SelenideElement filterPriceBelow100 = $("#narrow-by-list dd li a[href*='-100']");
    public final SelenideElement filterPriceAbove100 = $("#narrow-by-list dd li a[href*='100-']");
    public final SelenideElement countAbove100Selector = $("#narrow-by-list dd li a[href*='100-'] .count");
    public final ElementsCollection product = $$(".product-info");
    public final ElementsCollection filterBySizeCount = $$("a[href*='size'] .count");
    public final SelenideElement nmbItem = $(".toolbar-bottom .count-container .amount");

    /**
     * Actions
     */
    @Step("Click on filter price {0}")
    public void clickFilterPrice(SelenideElement elementSelector) {
        elementSelector.click();
    }

    @Step("The number of products found in the filter {0}")
    public int getCount(SelenideElement countSelector) {
        String nmbOfItems = countSelector.getText().replaceAll("[^a-zA-Z0-9]", "");
        return Integer.parseInt(nmbOfItems);
    }

    @Step("Select color with index {0}")
    public void selectColor(int index) {
        for (int i = 0; i < color.size(); i++) {
            color.get(index).click();
        }
    }

    public String getColorFromFilter(int index) {
        String colorName = "";
        for (int i = 0; i < color.size(); i++) {
            colorName = color.get(index).getAttribute("alt").toLowerCase();
        }
        return colorName;
    }

    @Step("Check if the color selected {0} is in results")
    public void checkFilteredColor(String colorFilter, ElementsCollection product) {
        for (int i = 0; i < product.size(); i++) {
            List<WebElement> productColors = product.get(i).findElements(By.cssSelector("img"));
            List<String> colorName = new ArrayList<>();
            for (int j = 0; j < productColors.size(); j++) {
                colorName.add(productColors.get(j).getAttribute("alt"));
            }
            Assert.assertTrue(colorName.contains(colorFilter));
        }
    }

    public int getNmbItemsFiltered(SelenideElement selector) {
        return Integer.parseInt(selector.getText().replaceAll("[^a-zA-Z0-9]", "").replaceAll("Items", ""));
    }

    @Step("Chose the size to filter the products")
    public void selectSize(int index) {
        filterBySizeCount.get(index).click();
    }
}

