package homepage;

/**
 * Object product in cart
 */
public class CheckOutProductInfo extends Product {

    private String shippingAndHandling;
    private String grandTotal;

    public CheckOutProductInfo(String name, String price, String color, String size, String quantity, String shippingAndHandling, String grandTotal) {
        super(name, price, color, size, quantity);
        this.shippingAndHandling = shippingAndHandling;
        this.grandTotal = grandTotal;
    }

    public String getShippingAndHandling() {
        return shippingAndHandling;
    }

    public String getGrandTotal() {
        return grandTotal;
    }
}
