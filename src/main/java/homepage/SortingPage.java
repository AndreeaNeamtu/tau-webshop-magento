package homepage;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SortingPage extends ScreenShooter {

    /**
     * Selectors
     */

    private final ElementsCollection nameProductsList = $$(".product-info .product-name");
    public final ElementsCollection productsCategory = $$("#nav li .level0:not(ul)");
    public final ElementsCollection priceProductsList = $$(".price-box [id*='product-price']");

    public final SelenideElement salesCategory = $(".has-children[href*='sale']");
    public final SelenideElement sortDropdown = $(".toolbar-bottom .sort-by select");
    public final SelenideElement descendingSorting = $(".toolbar-bottom .sort-by a[href*='dir=desc']");


    /**
     * Actions
     */

    public List<String> getListOfProductsName() {
        List<String> list = new ArrayList();
            ElementsCollection elements = nameProductsList;
            for (SelenideElement el : elements) {
                list.add(el.text());
        }
        return list;
    }

    public List<Double> getPriceList() {
        List<Double> list = new ArrayList();
        ElementsCollection elements = priceProductsList;
        for (SelenideElement el : elements) {
            String newPrice = el.getText().replaceAll(" LEU", "").replaceAll(",", ".");
            double price = Double.parseDouble(newPrice);
            list.add(price);
        }
        return list;
    }

    @Step("Choose one category of products")
    public void chooseOneCategory(String category) {
        for (int i = 0; i < productsCategory.size(); i++) {
            if (productsCategory.get(i).getText().equalsIgnoreCase(category))
                productsCategory.get(i).click();
        }
    }

    @Step("Click the arrow to select descending order")
    public void selectDescending() {
        descendingSorting.click();
    }

}




