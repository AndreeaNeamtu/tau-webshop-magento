package homepage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.Select;
import setup.ScreenShooter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CheckOutPage extends ScreenShooter {

    /**
     * Selectors
     */
    public final SelenideElement firstNameInput = $(".input-box input[name='billing[firstname]']");
    public final SelenideElement lastNameInput = $(".input-box input[name='billing[lastname]']");
    public final SelenideElement emailInput = $(".input-box input[name='billing[email]']");
    public final ElementsCollection addressInput = $$(".input-box input[name='billing[street][]']");
    public final SelenideElement cityInput = $(".input-box input[name='billing[city]']");
    public final SelenideElement zipCodeInput = $(".input-box input[name='billing[postcode]']");
    public final SelenideElement telephoneInput = $(".input-box input[name='billing[telephone]']");
    public final SelenideElement stateDropdown = $(".input-box select[name='billing[region_id]']");
    public final SelenideElement countryDropdown = $(".input-box select[name='billing[country_id]']");
    public final SelenideElement ajaxImgBilling = $("#billing-buttons-container .please-wait img");
    public final SelenideElement ajaxImgShippingMethod = $("#shipping-method-buttons-container .please-wait img");
    public final SelenideElement ajaxImgPayment = $("#payment-buttons-container .please-wait img");
    public final SelenideElement continueBillingInformationButton = $("#billing-buttons-container button");
    public final SelenideElement continuePaymentBtn = $("#payment-buttons-container button");
    public final SelenideElement continueShippingBtn = $("#shipping-method-buttons-container button");
    public final SelenideElement continueAsGuestBtn = $("#onepage-guest-register-button");
    public final SelenideElement shippingAndHandling = $("#checkout-review-table tfoot tr:nth-child(2) .price");
    public final SelenideElement tax = $("#checkout-review-table tfoot tr:nth-child(3) .price");
    public final SelenideElement grandTotal = $("#checkout-review-table tfoot tr:nth-child(4) .price");
    public final SelenideElement productNameCheckOut = $("tbody .product-name");
    public final SelenideElement priceCheckOut = $("tbody td[data-rwd-label='Price'] .price");
    public final SelenideElement colorCheckout = $("td .item-options dd:nth-child(2)");
    public final SelenideElement sizeCheckout = $("td .item-options dd:nth-child(4)");
    public final SelenideElement quantityCheckout = $("tbody td[data-rwd-label='Qty']");
    public final SelenideElement shippingAndHandlingCheckout = $("tfoot > tr:nth-child(2) > td.a-right > span");
    public final SelenideElement taxCheckout = $("tfoot > tr:nth-child(3) > td.a-right > span");
    public final SelenideElement grandTotalCheckout = $("tfoot tr td.a-right > strong > span");
    public final SelenideElement orderConfirmation = $(".page-title h1");
    public final SelenideElement thankMessage = $(".sub-title");
    private final SelenideElement placeOrderBtn = $("#review-buttons-container button");
    public final SelenideElement billingAddress = $("#billing-progress-opcheckout address");

    /**
     * Actions
     */

    @Step("Complete first name: {0}")
    public void fillInFirstName(String input) {
        firstNameInput.sendKeys(input);
    }

    @Step("Complete last name: {0}")
    public void fillInLastName(String input) {
        lastNameInput.sendKeys(input);
    }

    @Step("Complete email address: {0}")
    public void fillInEmail(String input) {
        emailInput.sendKeys(input);
    }

    @Step("Complete address: {0}")
    public void fillInAddress(String input) {
        addressInput.get(0).sendKeys(input);
    }

    @Step("Complete city: {0}")
    public void fillInCity(String input) {
        cityInput.sendKeys(input);
    }

    @Step("Complete zip code: {0}")
    public void fillInZipCode(String input) {
        zipCodeInput.sendKeys(input);
    }

    @Step("Complete telephone: {0}")
    public void fillInTelephone(String input) {
        telephoneInput.sendKeys(input);
    }

    @Step("Select one item: {item}")
    public void dropdownSelectOption(SelenideElement selector, String item) {
        SelenideElement dropdownElement = selector;
        dropdownElement.click();
        Select option = new Select(dropdownElement);
        option.selectByVisibleText(item);
    }

    @Step("Wait to load next step")
    public void pleaseWait(SelenideElement ajaxImg) {
        ajaxImg.shouldBe(Condition.appear);
        ajaxImg.shouldBe(Condition.disappear);
    }

    @Step("Click on {continueBtn} button")
    public void clickContinueButton(SelenideElement continueBtn) {
        continueBtn.shouldBe(Condition.visible);
        continueBtn.click();
    }

    public CheckOutProductInfo getData() {
        return new CheckOutProductInfo(productNameCheckOut.getText(),
                priceCheckOut.getText(),
                colorCheckout.getText(),
                sizeCheckout.getText(),
                quantityCheckout.getText(),
                shippingAndHandlingCheckout.getText(),
                grandTotalCheckout.getText());
    }

    @Step("Click on Place Order button")
    public void clickPlaceOrder() {
        placeOrderBtn.click();
    }

}
