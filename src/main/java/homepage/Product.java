package homepage;

public class Product {

    String name;
    String price;
    String color;
    String size;
    String quantity;

    public Product(String name, String price, String color, String size, String quantity) {
        this.name = name;
        this.price = price;
        this.color = color;
        this.size = size;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getColor() {
        return color;
    }

    public String getSize() {
        return size;
    }

    public String getQuantity() {
        return quantity;
    }
}
