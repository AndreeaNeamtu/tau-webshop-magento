package homepage;

import com.codeborne.selenide.*;
import io.qameta.allure.Step;
import org.openqa.selenium.support.ui.Select;
import setup.ScreenShooter;

import java.util.Random;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class HomePage extends ScreenShooter {

    /**
     * Selectors
     */

    // selectors for header
    private final SelenideElement logo = $(".logo");
    private final SelenideElement welcomeMessage = $(".welcome-msg");
    public final SelenideElement account_button = $(".skip-account");
    public final ElementsCollection accountDropdown = $$("#header-account ul li");
    private final SelenideElement navBar = $("#nav");

    //selector for main container
    private final SelenideElement mainContainer = $(".main-container");
    public final SelenideElement slideshow = $(".slideshow-container");

    // selectors for footer
    public final ElementsCollection footerElements = $$(".footer-container .links");
    public final SelenideElement newsletterInput = $("#newsletter");
    public final SelenideElement subscribeButton = $(".actions .button[type='submit']");
    private final SelenideElement messageSubscribe = $(".success-msg span");
    public final SelenideElement copywriter = $(".footer .copyright");
    public final ElementsCollection linksTo = $$(".footer .links ul li a");

    // selectors for products category
    public final SelenideElement jewelryCategory = $(".level1 a[href*='jewelry']");
    public final SelenideElement pantsCategory = $(".level1 a[href*='pants-denim']");
    public final SelenideElement kidsShoesCategory = $(".level1 a[href*='kids-shoes']");
    private final ElementsCollection category = $$("#nav li.level0");
    public final SelenideElement kitchenCategory = $(".level1 a[href*='kitchen']");
    public final SelenideElement vipCategory = $(".nav-primary .level0 a[href*='vip']");

    //selector for search
    public final SelenideElement searchInput = $("#search");
    public final SelenideElement searchButton = $(".search-button");

    /**
     * Actions
     */

    @Step("Open Page with screenshot")
    public void openPage() {
        Configuration.startMaximized = true;
        open("http://testfasttrackit.info/magento-test/");
        takeScreenShot("Open Page");
    }

    public void closePage() {
        closeWindow();
    }

    @Step("Click on Subscribe")
    public void subscribeNewsletter() {
        subscribeButton.click();
    }

    @Step("Write email in newsletter input: {0}")
    public void fillInNewsletter(String input) {
        newsletterInput.sendKeys(input);
    }

    @Step("Verify the main keys on Home Page")
    public void verifyHomePage() {
        logo.shouldBe(visible);
        verifyWelcomeMessageByText("WELCOME");
    }

    @Step("Verify welcome message {0}")
    public void verifyWelcomeMessageByText(String welcomeText) {
        welcomeMessage.shouldHave(Condition.exactText(welcomeText));
        takeScreenShot("Welcome message");
    }

    @Step("Select \"{0}\" from account dropdown")
    public void clickOnMenuButton(String labelButton) {
        account_button.click();
        for (SelenideElement selenideElement : accountDropdown) {
            if (selenideElement.getText().equalsIgnoreCase(labelButton)) {
                selenideElement.click();
            break;}
        }
    }

    @Step("Select an item from account dropdown: list {0}, item {1}, for method : {method} step...")
    public void selectItemFromList(ElementsCollection list, String item) {
        for (SelenideElement selenideElement : list) {
            if (selenideElement.getText().equalsIgnoreCase(item))
                selenideElement.click();
        }
    }

    @Step("Select item \"{1}\" from dropdown")
    public void dropdownSelectOption(SelenideElement selector, String item) {
        SelenideElement dropdownElement = selector;
        dropdownElement.click();
        Select option = new Select(dropdownElement);
        option.selectByVisibleText(item);
    }

    @Step("Go to the chosen category: {0} and select {indexCategory}")
    public void goToCategory(SelenideElement categorySelector, int indexCategory) {
        mainContainer.shouldBe(visible);
        navBar.shouldBe(visible);
        navBar.shouldBe(enabled);
        category.get(indexCategory).hover();
        categorySelector.shouldBe(visible);
        categorySelector.should(enabled);
        categorySelector.click();
        takeScreenShot("category selected");
    }

    public int generateRandomNumberBetween(int a, int b) {
        Random ran = new Random();
        return ran.nextInt(b - a + 1) + a;
    }

    @Step("Write the word \"{0}\" to make a search")
    public void fillInSearch(String input) {
        searchInput.sendKeys(input);
    }

    @Step("Click on Search button")
    public void clickSearchButton() {
        searchButton.click();
    }

    /**
     * Validators
     */

    @Step("Logo is visible")
    public void logoIsVisible() {
        logo.shouldBe(visible);
        takeScreenShot("Visibility of logo");
    }

    @Step("Verify subscribe message: {0}")
    public void verifySubscribeMessageByText(String subscribeText) {
        messageSubscribe.shouldHave(Condition.exactText(subscribeText));
        takeScreenShot("Subscribe success");
    }

}
