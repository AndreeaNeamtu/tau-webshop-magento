package homepage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class RegisterPage extends ScreenShooter {

    /**
     * Selectors
     */

    private final SelenideElement registerButton = $(".buttons-set .button");
    private final SelenideElement firstNameInput = $("#firstname");
    private final SelenideElement lastNameInput = $("#lastname");
    private final SelenideElement emailAddress = $("#email_address");
    private final SelenideElement password = $("#password");
    private final SelenideElement confirmPassword = $("#confirmation");
    private final SelenideElement registerSuccessMessage = $(".success-msg span");
    private final SelenideElement registerErrorMessage = $(".error-msg span");
    private final ElementsCollection messageRequiredField = $$(".validation-advice");
    public final ElementsCollection requiredField = $$(".form-list .required-entry");

    /**
     * Validators
     */

    @Step("Register button is visible")
    public void registerButtonIsVisible() {
        registerButton.shouldBe(visible);
    }

    @Step("Verify message when you register with success")
    public void verifyRegisterMessage(String messageText) {
        registerSuccessMessage.shouldBe(visible);
        registerSuccessMessage.shouldHave(Condition.exactText(messageText));
        takeScreenShot("Type of message when you register");
    }

    @Step("Verify message when you register with error")
    public void wrongRegisterMessage(String messageText) {
        registerErrorMessage.shouldBe(visible);
        registerErrorMessage.shouldHave(Condition.exactText(messageText));
    }

    @Step("Verify if message required field appear")
    public void verifyMessageRequiredField(String message) {
        for (SelenideElement selenideElement : messageRequiredField) {
            selenideElement.shouldBe(visible);
            selenideElement.shouldHave(exactText(message));
        }
    }

    /**
     * Actions
     */

    @Step("Complete the form with first name: {0}")
    public void fillInFirstName(String input) {
        firstNameInput.sendKeys(input);
    }

    @Step("Complete the form with last name: {0}")
    public void fillInLastName(String input) {
        lastNameInput.sendKeys(input);
    }

    @Step("Complete the form with email address: {0}")
    public void fillInEmailAddress(String input) {
        emailAddress.sendKeys(input);
    }

    @Step("Complete the form with password: {0}")
    public void fillInPassword(String input) {
        password.sendKeys(input);
    }

    @Step("Complete the form with password to confirm {0}")
    public void fillInConfirmPassword(String input) {
        confirmPassword.sendKeys(input);
    }

    @Step("Click on register button")
    public void clickOnRegisterButton() {
        registerButton.click();
    }
}
