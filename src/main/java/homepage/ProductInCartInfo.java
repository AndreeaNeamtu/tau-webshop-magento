package homepage;

public class ProductInCartInfo extends Product{

    String sku;

    public ProductInCartInfo(String name, String price, String color, String size, String quantity, String sku) {
        super(name, price, color, size, quantity);
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }
}
