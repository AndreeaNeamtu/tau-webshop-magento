package homepage;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage extends ScreenShooter {

    /**
     * Selectors
     */
    private final SelenideElement nmbOfItemInCart = $(".header-minicart .count");
    public final SelenideElement messageAddToCartSuccess = $(".success-msg span");
    public final SelenideElement emptyCartMessage = $(".page-title h1");
    private final SelenideElement emptyCartBtn = $("#empty_cart_button");
    public final SelenideElement input = $(".first td.product-cart-actions input");
    private final SelenideElement updateBtn = $("td.product-cart-actions button[style*='display']");
    private final ElementsCollection deleteBtn = $$("td.product-cart-remove .btn-remove");
    private final SelenideElement proceedToCheckoutBtn = $(".cart-totals .btn-proceed-checkout");
    public final ElementsCollection productsName = $$("#shopping-cart-table tbody tr");
    public final SelenideElement productNameInCart = $(".product-cart-info .product-name a");
    public final SelenideElement priceInCart = $(".product-cart-price .cart-price .price");
    public final SelenideElement colorInCart = $(".product-cart-info .item-options dd:nth-child(2)");
    public final SelenideElement sizeInCart = $(".product-cart-info .item-options dd:nth-child(4)");
    public final SelenideElement quantityInCart = $(".product-cart-actions input");
    public final SelenideElement skuInCart = $(".product-cart-sku");
    public final SelenideElement countryDropdown = $("#country");
    public final SelenideElement stateDropdown = $("#region_id");
    private final SelenideElement subtotal = $(".product-cart-total .cart-price span");
    private final SelenideElement tax = $("tbody tr:nth-child(2) td .price");
    private final SelenideElement estimateButton = $(".buttons-set button");
    public final SelenideElement flatRate = $(".shipping-form .sp-methods li label");
    private final SelenideElement priceFlatRate = $(".shipping-form .sp-methods li label span");
    public final SelenideElement flatRateRadiobutton = $(".shipping-form .sp-methods li input");
    private final SelenideElement total = $("tfoot tr:nth-child(1) td .price");
    public final SelenideElement shoppingCartTable = $("#shopping-cart-table");


    /**
     * Actions
     */
    public String getNumberOfItemsFromCart() {
        String nmbOfItem = nmbOfItemInCart.getText();
        return nmbOfItem;
    }

    @Step("Click on Empty cart button")
    public void emptyCart() {
        emptyCartBtn.click();
        takeScreenShot("Cart is empty");
    }

    @Step("Change quantity with: {quantity}")
    public void writeInInput(String quantity) {
        input.click();
        input.clear();
        input.sendKeys(quantity);
    }

    @Step("Click on Update Button")
    public void clickUpdateBtn() {
        updateBtn.click();
        takeScreenShot("Update cart");
    }

    @Step("Click on Delete Button")
    public void clickOnDeleteButton() {
        deleteBtn.get(1).click();
    }

    @Step("Click on Proceed to Checkout button")
    public void clickProceedToCheckout() {
        proceedToCheckoutBtn.click();
        takeScreenShot("Proceed to checkout");
    }

    public ProductInCartInfo getProductInCartInfo() {
        return new ProductInCartInfo(productNameInCart.getText(),
                priceInCart.getText(),
                colorInCart.getText(),
                sizeInCart.getText(),
                quantityInCart.getAttribute("value"),
                skuInCart.getText());
    }

    @Step("Write in city input: {0}")
    public void filInCityInput(String input) {
        SelenideElement cityInput = $(".input-box #city");
        cityInput.click();
        cityInput.sendKeys(input);
    }

    @Step("Write in zip code input: {0}")
    public void filInZipInput(String input) {
        SelenideElement cityInput = $(".input-box #postcode");
        cityInput.click();
        cityInput.sendKeys(input);
    }

    public double getSubtotalInCart() {
        return Double.parseDouble(subtotal.getText().replaceAll(" LEU", "").replaceAll(",", "."));
    }

    public double getTaxInCart() {
        return Double.parseDouble(tax.getText().replaceAll(" LEU", "").replaceAll(",", "."));
    }

    @Step("Click on Estimate button")
    public void clickEstimate() {
        estimateButton.click();
    }

    public double getFlatRatePrice() {
        String priceFR = priceFlatRate.getText().replaceAll(" LEU", "").replaceAll(",", ".");
        return Double.parseDouble(priceFR);
    }

    public double getTotalPrice() {
        return Double.parseDouble(total.getText().replaceAll(" LEU", "").replaceAll(",", "."));
    }

    @Step("Select Flat Rate")
    public void selectRadioButton() {
        flatRateRadiobutton.click();
    }

    @Step("Click Update Total button")
    public void clickUpdateTotal() {
        SelenideElement updateTotalButton = $(".buttons-set button[title='Update Total']");
        updateTotalButton.click();
    }
}
