package homepage;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LoginPage extends ScreenShooter {

    /**
     * Selectors
     */

    private final SelenideElement emailInput = $(".input-box #email");
    private final SelenideElement passwordInput = $(".input-box #pass");
    private final SelenideElement loginButton = $(".buttons-set #send2");
    private final SelenideElement welcomeMessage = $(".my-account .hello");
    private final SelenideElement emailRequiredField = $("#advice-required-entry-email");
    private final SelenideElement passwordRequiredField = $("#advice-required-entry-pass");

    /**
     * Validators
     */

    @Step("Login welcome message should be: \"{messageText}\"")
    public void verifyWelcomeMessage(String messageText) {
        welcomeMessage.shouldBe(visible);
        welcomeMessage.shouldHave(exactText(messageText));
        takeScreenShot("Login welcome message");
    }

    @Step("Check the message for required fields, message should be: \"{messageText}\"")
    public void verifyRequiredFieldsOnLogin(String messageText) {
        emailRequiredField.shouldBe(visible);
        emailRequiredField.shouldHave(exactText(messageText));
        passwordRequiredField.shouldBe(visible);
        passwordRequiredField.shouldHave(exactText(messageText));
        takeScreenShot("Required fields for login");
    }

    /**
     * Actions
     */

    @Step("Complete field with email address: {0}")
    public void fillInEmailAddress(String input) {
        emailInput.sendKeys(input);
    }

    @Step("Complete field with password: {0}")
    public void fillInPassword(String input) {
        passwordInput.sendKeys(input);
    }

    @Step("Click on Login button")
    public void clickOnLoginButton() {
        loginButton.click();
    }


}
