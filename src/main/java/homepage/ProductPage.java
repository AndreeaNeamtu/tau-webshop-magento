package homepage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.apache.commons.lang3.text.WordUtils.capitalizeFully;

public class ProductPage extends ScreenShooter {

    HomePage homePage = new HomePage();

    /**
     * Selectors
     */
    private final ElementsCollection addToCartBtns = $$(".product-info .actions button");
    private final SelenideElement addToCartBtn = $(".add-to-cart-buttons button");
    public final ElementsCollection requiredField = $$("[id*='advice-required-entry-attribute']");
    private final ElementsCollection productsName = $$(".category-products ul .item .product-name a");
    private final ElementsCollection productName = $$(".products-grid .item .product-name a");
    private final ElementsCollection size = $$("#configurable_swatch_size li");
    private final ElementsCollection color = $$("#configurable_swatch_color img");
    public final SelenideElement quantityInput = $("#qty");
    public final SelenideElement messageSuccess = $(".success-msg span");
    public final SelenideElement name = $(".product-shop .product-name span");
    public final SelenideElement errorMsg = $("#messages_product_view span");
    public final SelenideElement colorSelected = $("#select_label_color");
    public final SelenideElement price = $(".price-info .price-box .regular-price .price");
    public final SelenideElement sizeSelected = $("#select_label_size");
    public final SelenideElement addToWishlistButton = $("a.link-wishlist");
    public final ElementsCollection addToCompareLinks = $$(".actions .add-to-links li a.link-compare");
    private final SelenideElement compareBtn = $(".block-compare .block-content .actions button");
    public final ElementsCollection numberOfItemInCompareBlock = $$("#compare-items .product-name a");
    public final SelenideElement compareBlock = $(".block-compare");
    public final SelenideElement priceAfterDiscount = $(".product-pricing .tier-price .price");
    private final SelenideElement reviews = $(".toggle-tabs .last");
    private final ElementsCollection qualityRadioButtons = $$("tbody tr td.value label input[id*='Quality']");
    private final ElementsCollection deliveryRadioButtons = $$("tbody tr td.value label input[id*='Delivery']");
    private final ElementsCollection priceRadioButtons = $$("tbody tr td.value label input[id*='Price']");
    private final ElementsCollection designRadioButtons = $$("tbody tr td.value label input[id*='Design']");
    private final ElementsCollection valueRadioButtons = $$("tbody tr td.value label input[id*='Value']");
    private final SelenideElement review = $(".form-list .input-box #review_field");
    private final SelenideElement summary = $(".inline-label .input-box #summary_field");
    private final SelenideElement nickName = $(".inline-label .input-box #nickname_field");
    private final SelenideElement submitReviewButton = $(".buttons-set button");
    public final SelenideElement message = $(".success-msg li span");
    public final SelenideElement addReview = $("#customer-reviews a[href*='review-form']");
    public final SelenideElement reviewForm = $(".fieldset");
    public final ElementsCollection nameInCompareWindows= $$("tbody .product-shop-row td h2 a");
    private final SelenideElement ok = $(".buttons-set button");

    /**
     * Actions
     */

    @Step("Add product with index {index} to the cart")
    // add a product to the cart directly from category page
    public void addToCart(int index) {
        addToCartBtns.get(index).click();
    }

    @Step("Add product to the cart")
    // add a product to the cart from its page
    public void addToCart() {
        addToCartBtn.click();
    }

    @Step("Choose the product: {nmbOfItem}")
    public void selectItem(int nmbOfItem) {
        productsName.get(nmbOfItem).click();
    }

    public String getProductName() {
        String productName = name.getText();
        String newProductName = capitalizeFully(productName);
        return newProductName;
    }

    @Step("Select size")
    public void selectSize() {
        int nmb = homePage.generateRandomNumberBetween(0, size.size());
        for (int i = 0; i < size.size(); i++) {
            if (i == nmb) {
                size.get(i).click();
            }
        }
    }

    @Step("Select color")
    public void selectColor() {
        homePage.generateRandomNumberBetween(0, color.size());
        for (SelenideElement selenideElement : color) {
            selenideElement.click();
        }
    }

    @Step("Change quantity of product with: {0}")
    public String addQuantity(String quantity) {
        quantityInput.clear();
        quantityInput.sendKeys(quantity);
        return quantity;
    }

    public String getProductName(int index) {
        String productName = "";
        for (int i = 0; i < productsName.size(); i++) {
            if (i == index) {
                productName = productsName.get(i).getAttribute("title");
            }
        }
        return productName;
    }

    @Step("The product name after search")
    public List<String> getProductNameAfterSearch() {
        List<String> list = new ArrayList();
        ElementsCollection elements = productName;
        for (SelenideElement el : elements) {
            list.add(el.getAttribute("title"));
        }
        return list;
    }

    public Product getProductInfo() {
        return new Product(name.getText(),
                price.getText(),
                colorSelected.getText(),
                sizeSelected.getText(),
                quantityInput.getAttribute("value"));
    }

    @Step("Click 'Add to Wishlist' link")
    public void clickAddToWishlist() {
        addToWishlistButton.click();
    }

    @Step("Click on Compare button")
    public void clickCompareButton() {
        compareBtn.click();
    }

    @Step("Click on Reviews")
    public void clickOnReviews() {
        reviews.click();
    }

    @Step("Rate quality")
    public void rateQuality() {
        for (SelenideElement qualityRadioButton : qualityRadioButtons) {
            qualityRadioButton.shouldBe(Condition.visible);
        }
        int quality = homePage.generateRandomNumberBetween(0, qualityRadioButtons.size() - 1);
        qualityRadioButtons.get(quality).click();
        qualityRadioButtons.get(quality).shouldBe(Condition.selected);
    }

    @Step("Rate delivery")
    public void rateDelivery() {
        for (SelenideElement deliveryRadioButton : deliveryRadioButtons) {
            deliveryRadioButton.shouldBe(Condition.visible);
        }
        int delivery = homePage.generateRandomNumberBetween(0, deliveryRadioButtons.size() - 1);
        deliveryRadioButtons.get(delivery).click();
        deliveryRadioButtons.get(delivery).shouldBe(Condition.selected);

    }

    @Step("Rate price")
    public void ratePrice() {
        for (SelenideElement priceRadioButton : priceRadioButtons) {
            priceRadioButton.shouldBe(Condition.visible);
        }
        int price = homePage.generateRandomNumberBetween(0, priceRadioButtons.size() - 1);
        priceRadioButtons.get(price).click();
        priceRadioButtons.get(price).shouldBe(Condition.selected);
    }

    @Step("Rate design")
    public void rateDesign() {
        for (SelenideElement designRadioButton : designRadioButtons) {
            designRadioButton.shouldBe(Condition.visible);
        }
        int design = homePage.generateRandomNumberBetween(0, designRadioButtons.size() - 1);
        designRadioButtons.get(design).click();
        designRadioButtons.get(design).shouldBe(Condition.selected);
    }

    @Step("Rate value")
    public void rateValue() {
        for (SelenideElement valueRadioButton : valueRadioButtons) {
            valueRadioButton.shouldBe(Condition.visible);
        }
        int value = homePage.generateRandomNumberBetween(0, valueRadioButtons.size() - 1);
        valueRadioButtons.get(value).click();
        valueRadioButtons.get(value).shouldBe(Condition.selected);
    }

    @Step("Write the review: {0}")
    public void writeReview(String input) {
        review.click();
        review.sendKeys(input);
    }

    @Step("Write summary: {0}")
    public void writeSummary(String input) {
        summary.click();
        summary.sendKeys(input);
    }

    @Step("Write nickname: {0}")
    public void writeNickName(String input) {
        nickName.click();
        nickName.sendKeys(input);
    }

    @Step("Click on Submit Review button")
    public void clickOnSubmit() {
        submitReviewButton.click();
    }

    @Step("Click Add to Compare list product: {0}")
    public void addToCompareList(int i) {
        addToCompareLinks.get(i).click();
    }

    @Step("Click on close Window")
    public void clickCloseWindow() {
        ok.click();
    }
}

