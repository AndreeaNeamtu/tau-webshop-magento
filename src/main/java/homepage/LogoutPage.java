package homepage;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import setup.ScreenShooter;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class LogoutPage extends ScreenShooter {

    /**
     * Selectors
     */

    private final SelenideElement logoutMessage = $(".page-title h1");
    private final SelenideElement redirectMessage = $(".main-container p");

    /**
     * Validators
     */

    @Step("Logout message should be: \"{messageText}\" and the redirection message should be: \"{redirectionMessage}\"")
    public void verifyLogoutMessage(String messageText, String redirectionMessage) {
        logoutMessage.shouldBe(visible);
        logoutMessage.shouldHave(exactText(messageText));
        redirectMessage.shouldBe(visible);
        redirectMessage.shouldHave(exactText(redirectionMessage));
        takeScreenShot("Logout message");
    }
}
