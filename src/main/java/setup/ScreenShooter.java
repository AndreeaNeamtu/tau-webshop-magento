package setup;

import io.qameta.allure.Allure;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.UUID;

import static com.codeborne.selenide.Selenide.screenshot;

public class ScreenShooter {

    //method to take screenshot
    public void takeScreenShot(String name) {
        try {
            Allure.addAttachment("PrintScreen " + name,
                    Files.newInputStream(Paths.get(URI.create(Objects.requireNonNull(screenshot(UUID.randomUUID().toString()))))));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
