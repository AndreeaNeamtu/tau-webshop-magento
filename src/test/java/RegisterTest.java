import homepage.HomePage;
import homepage.RegisterPage;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

@Epic("Register Tests")
@Test(description = "Register tests")
public class RegisterTest {
    HomePage homePage;
    RegisterPage registerPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        registerPage = new RegisterPage();
        screenShooter = new ScreenShooter();
    }

    /**
     * this test fails because the user is already registered
     * to perform the test please introduce another credentials
     */
    @Feature("Register")
    @Test(description = "Register on Madison Island with valid credentials")
    public void register_with_valid_credentials() {
        homePage.openPage();

        homePage.clickOnMenuButton("Register");
        registerPage.registerButtonIsVisible();

        registerPage.fillInFirstName("Vasile");
        registerPage.fillInLastName("Ion");
        registerPage.fillInEmailAddress("vasile@dom.com");
        registerPage.fillInPassword("abc123");
        registerPage.fillInConfirmPassword("abc123");

        registerPage.clickOnRegisterButton();

        registerPage.verifyRegisterMessage("Thank you for registering with Madison Island.");

        screenShooter.takeScreenShot("Register success");

        homePage.closePage();

    }


    @Feature("Register")
    @Test(description = "Register on Madison Island with a user already register")
    public void register_with_user_registered() {
        homePage.openPage();

        homePage.clickOnMenuButton("Register");
        registerPage.registerButtonIsVisible();

        registerPage.fillInFirstName("Vasile");
        registerPage.fillInLastName("Ion");
        registerPage.fillInEmailAddress("vasile@dom.com");
        registerPage.fillInPassword("abc123");
        registerPage.fillInConfirmPassword("abc123");
        registerPage.clickOnRegisterButton();
        registerPage.wrongRegisterMessage("There is already an account with this email address." +
                " If you are sure that it is your email address, click here to get your password and access your account.");

        screenShooter.takeScreenShot("Register with a user registered");

        homePage.closePage();
    }

    @Feature("Register")
    @Test(description = "Register without introducing information")
    public void register_without_information() {
        homePage.openPage();

        homePage.clickOnMenuButton("Register");
        registerPage.clickOnRegisterButton();

        //verify after message
        registerPage.verifyMessageRequiredField("This is a required field.");

        screenShooter.takeScreenShot("Required fields");

        homePage.closePage();
    }

    @Feature("Register")
    @Test(description = "Register on Madison Island with valid credentials without clicking Register button")
    public void register_with_valid_credentials_without_click_registerBtn() {

        homePage.openPage();

        homePage.clickOnMenuButton("Register");

        registerPage.registerButtonIsVisible();

        registerPage.fillInFirstName("Vasile");
        registerPage.fillInLastName("Ion");
        registerPage.fillInEmailAddress("vasile@dom.com");
        registerPage.fillInPassword("abc123");
        registerPage.fillInConfirmPassword("abc123");

        screenShooter.takeScreenShot("Fill in register form");

        homePage.closePage();
    }
}
