import homepage.HomePage;
import homepage.LoginPage;
import io.qameta.allure.Epic;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("Login Tests")
@Test(description = "Login tests")
public class LoginTest {
    HomePage homePage;
    LoginPage loginPage;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        loginPage = new LoginPage();
    }

    @Test(description = "Login with valid credentials")
    public void login_with_valid_credentials() {

        homePage.openPage();

        homePage.clickOnMenuButton("Log In");
        loginPage.fillInEmailAddress("vasile@dom.com");
        loginPage.fillInPassword("abc123");
        loginPage.clickOnLoginButton();

        String welcomeMessage = "Hello, Vasile Ion!";
        loginPage.verifyWelcomeMessage(welcomeMessage);

        homePage.closePage();
    }

    @Test(description = "Login with empty fields")
    public void login_with_empty_inputs() {
        homePage.openPage();

        homePage.clickOnMenuButton("Log In");
        loginPage.clickOnLoginButton();

        loginPage.verifyRequiredFieldsOnLogin("This is a required field.");

        homePage.closePage();
    }
}
