import homepage.HomePage;
import homepage.ProductPage;
import homepage.SortingPage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import java.util.List;

@Epic("Search Test")
@Test(description = "Search test")
public class SearchTest {
    HomePage homePage;
    SortingPage sortingPage;
    ProductPage productPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        sortingPage = new SortingPage();
        productPage = new ProductPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Search product after a given word")
    public void make_a_search() {
        homePage.openPage();

        String wordToSearch = "hana";

        homePage.fillInSearch(wordToSearch);

        homePage.clickSearchButton();

        List<String> productsName = productPage.getProductNameAfterSearch();

        for (String s : productsName) {
            Assert.assertTrue(s.toLowerCase().contains(wordToSearch));
        }

        screenShooter.takeScreenShot("Items after search");

        homePage.closePage();
    }
}
