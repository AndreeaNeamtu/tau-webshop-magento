import homepage.HomePage;
import homepage.SortingPage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import java.util.Collections;
import java.util.List;

@Epic("Sorting Tests")
@Test(description = "Sorting tests")
public class SortingTest {

    HomePage homePage;
    SortingPage sortingPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        sortingPage = new SortingPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Sort ascending the products name")
    public void ascending_sort_name() {

        homePage.openPage();

        // click on "Sales" category
        homePage.goToCategory(sortingPage.salesCategory, 5);

        // get initial list of products name
        List<String> nameProductsBeforeSort = sortingPage.getListOfProductsName();

        screenShooter.takeScreenShot("Products before sorting by price");

        // select option to sort by
        homePage.dropdownSelectOption(sortingPage.sortDropdown, "Name");

        //get the list of products names after sort
        List<String> nameProductsAfterSort = sortingPage.getListOfProductsName();

        // sort the initial list of products
        Collections.sort(nameProductsBeforeSort);

        screenShooter.takeScreenShot("Products after sorting by price");

        Assert.assertEquals(nameProductsBeforeSort, nameProductsAfterSort, "The products are sorted ascending by name");

        homePage.closePage();
    }


    @Test(description = "Sort descending the products name")
    public void descending_sort_by_name() {

        homePage.openPage();

        // click on "Sale" category
        homePage.goToCategory(sortingPage.salesCategory, 5);

        // get initial list of products name
        List<String> nameProductsBeforeSort = sortingPage.getListOfProductsName();

        screenShooter.takeScreenShot("Products before sorting");

        // select option to sort by
        homePage.dropdownSelectOption(sortingPage.sortDropdown, "Name");

        // set descending option
        sortingPage.selectDescending();

        //get initial list of products names
        List<String> nameProductsAfterSort = sortingPage.getListOfProductsName();

        // sort the initial list of products
        Collections.sort(nameProductsBeforeSort, Collections.reverseOrder());

        screenShooter.takeScreenShot("Products after sorting");

        Assert.assertEquals(nameProductsBeforeSort, nameProductsAfterSort, "The products are sorted descending by name");

        homePage.closePage();
    }

    @Test(description = "Sort ascending by price")
    public void ascending_sort_by_price() {
        homePage.openPage();

        // click on "Sale" category
        homePage.goToCategory(sortingPage.salesCategory, 5);

        //get initial list of prices
        List<Double> pricesBeforeSort = sortingPage.getPriceList();

        screenShooter.takeScreenShot("Products before sorting");

        // select option to sort by
        homePage.dropdownSelectOption(sortingPage.sortDropdown, "Price");

        //get the price list after choosing the type of sorting
        List<Double> pricesAfterSort = sortingPage.getPriceList();

        // sort the initial list prices
        Collections.sort(pricesBeforeSort);

        screenShooter.takeScreenShot("Products after sorting");

        Assert.assertEquals(pricesBeforeSort, pricesAfterSort, "The products are sorted ascending by name");

        homePage.closePage();
    }

    @Test(description = "Sort descending by price")
    public void descending_sort_by_price() {
        homePage.openPage();

        // click on "Sale" category
        homePage.goToCategory(sortingPage.salesCategory, 5);

        //get initial list of prices
        List<Double> pricesBeforeSort = sortingPage.getPriceList();

        screenShooter.takeScreenShot("Products before sorting");

        // select option to sort by
        homePage.dropdownSelectOption(sortingPage.sortDropdown, "Price");

        // set descending option
        sortingPage.selectDescending();

        //get the price list after choosing the type of sorting
        List<Double> pricesAfterSort = sortingPage.getPriceList();

        // sort the initial list prices
        Collections.sort(pricesBeforeSort, Collections.reverseOrder());

        screenShooter.takeScreenShot("Products after sorting");

        Assert.assertEquals(pricesBeforeSort, pricesAfterSort, "The products are sorted ascending by name");

        homePage.closePage();
    }
}
