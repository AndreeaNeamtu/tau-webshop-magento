import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import homepage.HomePage;
import homepage.ProductPage;
import homepage.SortingPage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;

@Epic("Compare Products Test")
@Test(description = "Compare products test")
public class CompareProductsTest {

    HomePage homePage;
    ProductPage productPage;
    SortingPage sortingPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        sortingPage = new SortingPage();
        productPage = new ProductPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Check the functionality of Compare products")
    public void compare_2_products() {
        homePage.openPage();

        homePage.goToCategory(homePage.kidsShoesCategory, 7);

        List<String> productsName = sortingPage.getListOfProductsName();

        productPage.addToCompareList(0);

        //check the message
        productPage.messageSuccess.shouldBe(Condition.visible);
        productPage.messageSuccess.shouldHave(Condition.exactText("The product " + productsName.get(0) + " has been added to comparison list."));

        productPage.addToCompareList(1);

        productPage.messageSuccess.shouldBe(Condition.visible);
        productPage.messageSuccess.shouldHave(Condition.exactText("The product " + productsName.get(1) + " has been added to comparison list."));

        productPage.compareBlock.shouldBe(Condition.visible);

        // how many items are in compare
        Assert.assertEquals(productPage.numberOfItemInCompareBlock.size(), 2);

        productPage.clickCompareButton();

        switchTo().window(1);

        screenShooter.takeScreenShot("Compare Window");

        //check if the product name is in the compare window
        for (int i = 0; i < productPage.nameInCompareWindows.size(); i++) {
            productPage.nameInCompareWindows.get(i).shouldHave(Condition.exactText(productsName.get(i)));
        }

        productPage.clickCloseWindow();

        switchTo().window(0);

        homePage.closePage();
    }



}
