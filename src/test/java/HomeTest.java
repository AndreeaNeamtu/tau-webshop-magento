import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import homepage.HomePage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import static com.codeborne.selenide.Selenide.actions;

@Epic("Home Page Tests")
@Test(description = "Magento Shop HomePage test suites")
public class HomeTest {

    HomePage homePage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Logo is visible on main page")
    public void logo_is_visible() {
        homePage.openPage();
        homePage.logoIsVisible();
        homePage.closePage();
    }

    @Test(description = "Subscribe on newsletter")
    public void newsletter_subscribe() {
        homePage.openPage();
        homePage.fillInNewsletter("abc@yahoo.com");
        homePage.subscribeNewsletter();
        String messageSubscribe = "Thank you for your subscription.";
        homePage.verifySubscribeMessageByText(messageSubscribe);
        homePage.closePage();
    }

    @Test(description = "Check the footer container")
    public void footer_is_visible() {

        homePage.openPage();

        // 4 containers with links
        homePage.footerElements.shouldHave(CollectionCondition.size(4));

        // check the visibility of newsletter
        homePage.newsletterInput.shouldBe(Condition.visible);
        homePage.subscribeButton.shouldBe(Condition.visible);
        homePage.subscribeButton.shouldBe(Condition.enabled);

        // how many links are and check them
        homePage.linksTo.shouldHave(CollectionCondition.size(14));

        String[] links = {"About Us", "Contact Us", "Customer Service", "Privacy Policy", "Site Map", "Search Terms",
                "Advanced Search", "My Account", "Orders and Returns", "Facebook", "Twitter", "YouTube", "Pinterest", "RSS"};
        for (int i = 0; i < homePage.linksTo.size(); i++) {
            homePage.linksTo.get(i).shouldBe(Condition.visible);
            homePage.linksTo.get(i).shouldHave(Condition.text(links[i].toUpperCase()));
            Assert.assertEquals(homePage.linksTo.get(i).getText(), links[i].toUpperCase());
        }

        // check the copywriter
        String copywriterMessage = "© 2014 Madison Island. All Rights Reserved.";

        homePage.copywriter.shouldBe(Condition.visible);
        homePage.copywriter.shouldHave(Condition.text(copywriterMessage));

        actions().moveToElement(homePage.copywriter).build().perform();

        screenShooter.takeScreenShot("Footer");

        homePage.closePage();
    }
}
