import com.codeborne.selenide.Condition;
import homepage.*;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import static com.codeborne.selenide.Selenide.sleep;

@Epic("End To End Test")
@Test(description = "Place an order test")
public class EndToEndTest {
    HomePage homePage;
    ProductPage productPage;
    CartPage cartPage;
    CheckOutPage checkOutPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        productPage = new ProductPage();
        cartPage = new CartPage();
        checkOutPage = new CheckOutPage();
        screenShooter = new ScreenShooter();

    }

    @Test(description = "Order one item with confirmation")
    public void order_one_item_flow() {

        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        productPage.selectItem(3);

        productPage.selectColor();
        productPage.colorSelected.shouldBe(Condition.visible);

        productPage.selectSize();
        productPage.sizeSelected.shouldBe(Condition.visible);

        Product product = productPage.getProductInfo();

        productPage.addToCart();

        sleep(1000);

        ProductInCartInfo productInCartInfo = cartPage.getProductInCartInfo();

        //check if is the selected product
        Assert.assertEquals(product.getName(), productInCartInfo.getName());
        Assert.assertEquals(product.getPrice(), productInCartInfo.getPrice());
        Assert.assertEquals(product.getColor(), productInCartInfo.getColor());
        Assert.assertEquals(product.getSize(), productInCartInfo.getSize());
        Assert.assertEquals(product.getQuantity(), productInCartInfo.getQuantity());

        cartPage.clickProceedToCheckout();

        //checkout as a guest
        checkOutPage.clickContinueButton(checkOutPage.continueAsGuestBtn);

        //introduce required information
        checkOutPage.fillInFirstName("Vasile");
        checkOutPage.fillInLastName("Ion");
        checkOutPage.fillInEmail("vasile@dom.com");
        checkOutPage.fillInAddress("Mihai Eminescu 5");
        checkOutPage.fillInCity("Bucuresti");
        checkOutPage.dropdownSelectOption(checkOutPage.stateDropdown, "California");
        checkOutPage.fillInZipCode("123456");
        checkOutPage.dropdownSelectOption(checkOutPage.countryDropdown, "Polonia");
        checkOutPage.fillInTelephone("0256123456");

        String firstName = checkOutPage.firstNameInput.getAttribute("value");
        String lastName = checkOutPage.lastNameInput.getAttribute("value");
        String telephone = checkOutPage.telephoneInput.getAttribute("value");
        String zipCode = checkOutPage.zipCodeInput.getAttribute("value");
        String address = checkOutPage.addressInput.get(0).getAttribute("value");
        String country = checkOutPage.countryDropdown.getText();
        String city = checkOutPage.cityInput.getAttribute("value");

        checkOutPage.clickContinueButton(checkOutPage.continueBillingInformationButton);

        checkOutPage.pleaseWait(checkOutPage.ajaxImgBilling);

        checkOutPage.clickContinueButton(checkOutPage.continueShippingBtn);

        checkOutPage.pleaseWait(checkOutPage.ajaxImgShippingMethod);

        checkOutPage.clickContinueButton(checkOutPage.continuePaymentBtn);

        checkOutPage.pleaseWait(checkOutPage.ajaxImgPayment);

        //check the billing information
        Assert.assertEquals(checkOutPage.billingAddress.getText().replaceAll("\n", ""),
                firstName + " " + lastName + address + city + ", " + zipCode + country + "T: " + telephone);

        //get information about product and delivery address, payment etc
        CheckOutProductInfo checkOutProductInfo = checkOutPage.getData();

        //check if is the same product from cart
        Assert.assertEquals(productInCartInfo.getName(), checkOutProductInfo.getName());
        Assert.assertEquals(productInCartInfo.getPrice(), checkOutProductInfo.getPrice());
        Assert.assertEquals(productInCartInfo.getColor(), checkOutProductInfo.getColor());
        Assert.assertEquals(productInCartInfo.getSize(), checkOutProductInfo.getSize());
        Assert.assertEquals(productInCartInfo.getQuantity(), checkOutProductInfo.getQuantity());

        checkOutPage.clickPlaceOrder();

        checkOutPage.orderConfirmation.shouldBe(Condition.visible);
        checkOutPage.orderConfirmation.shouldHave(Condition.exactText("Your order has been received."));

        checkOutPage.thankMessage.shouldBe(Condition.visible);
        checkOutPage.thankMessage.shouldHave(Condition.exactText("Thank you for your purchase!"));

        screenShooter.takeScreenShot("Place Order success");

        homePage.closePage();

    }
}
