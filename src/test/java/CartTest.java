import com.codeborne.selenide.Condition;
import homepage.*;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import static org.apache.commons.lang3.text.WordUtils.capitalizeFully;

@Epic("Cart Tests")
@Test(description = "Cart tests")
public class CartTest {
    HomePage homePage;
    ProductPage productPage;
    CartPage cartPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        productPage = new ProductPage();
        cartPage = new CartPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Check the functionality of 'Empty cart' button")
    public void empty_cart() {
        homePage.openPage();

        //add a few products to cart
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(2);
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(4);
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(0);

        screenShooter.takeScreenShot("Cart before pressing Empty cart button");

        cartPage.emptyCart();

        String message = "Shopping Cart is Empty";

        //check after message
        cartPage.emptyCartMessage.shouldHave(Condition.text(message));

        screenShooter.takeScreenShot("Cart after pressing Empty cart button");

        homePage.closePage();
    }

    @Test(description = "Check the functionality of delete button in cart")
    public void check_functionality_delete_button() {
        homePage.openPage();

        //add a few products to cart
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(2);
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(4);
        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.addToCart(0);

        int nmbOfItems = cartPage.productsName.size();

        screenShooter.takeScreenShot("Before delete item");

        cartPage.clickOnDeleteButton();

        Assert.assertEquals(cartPage.productsName.size(), nmbOfItems - 1);

        screenShooter.takeScreenShot("Item is deleted");

        homePage.closePage();

    }

    @Test(description = "Change quantity of one item in cart")
    public void edit_quantity_in_cart() {
        homePage.openPage();

        //a go to a category
        homePage.goToCategory(homePage.jewelryCategory, 2);

        //add directly to cart
        productPage.addToCart(2);

        screenShooter.takeScreenShot("Before update");

        String quantity = "5";

        cartPage.writeInInput(quantity);

        cartPage.clickUpdateBtn();

        cartPage.input.shouldHave(Condition.value(quantity));

        homePage.closePage();
    }

    @Test(description = "Estimate shipping/tax and update total")
    public void estimate_shipping_and_update_total() {
        homePage.openPage();

        //go to pants category
        homePage.goToCategory(homePage.pantsCategory, 1);

        //select item
        productPage.selectItem(3);

        productPage.selectColor();
        productPage.colorSelected.shouldBe(Condition.visible);

        productPage.selectSize();
        productPage.sizeSelected.shouldBe(Condition.visible);

        Product product = productPage.getProductInfo();

        productPage.addToCart();

        cartPage.shoppingCartTable.shouldBe(Condition.visible);

        ProductInCartInfo productInCartInfo = cartPage.getProductInCartInfo();

        // chek in cart if is the same product, after name and price
        Assert.assertEquals(product.getName(), productInCartInfo.getName());
        Assert.assertEquals(product.getPrice(), productInCartInfo.getPrice());

        //get subtotal
        double subtotalInCart = cartPage.getSubtotalInCart();

        homePage.dropdownSelectOption(cartPage.countryDropdown, "Spania");

        homePage.dropdownSelectOption(cartPage.stateDropdown, "Cordoba");

        cartPage.filInCityInput("Cluj");

        cartPage.filInZipInput("123456");

        cartPage.clickEstimate();

        cartPage.flatRate.shouldBe(Condition.visible);

        //get price flat rate
        double flatRatePrice = cartPage.getFlatRatePrice();

        cartPage.selectRadioButton();

        cartPage.clickUpdateTotal();

        double totalCart = cartPage.getTotalPrice();

        Assert.assertEquals(totalCart, (subtotalInCart + flatRatePrice));

        screenShooter.takeScreenShot("Cart");

        homePage.closePage();
    }


    @Test(description = "Buy 1 product with discount")
    public void product_discount() {
        homePage.openPage();

        homePage.goToCategory(homePage.vipCategory, 10);

        productPage.selectItem(3);

        //get name and discount price from product page
        String name = productPage.getProductName();
        String priceBefore = productPage.priceAfterDiscount.getText();

        screenShooter.takeScreenShot("Discount");

        productPage.addToCart();

        //get name and price from cart
        String productNameInCart = capitalizeFully(cartPage.productNameInCart.getText());
        String priceInCart = cartPage.priceInCart.getText();

        Assert.assertEquals(priceInCart, priceBefore);
        Assert.assertEquals(name, productNameInCart);

        screenShooter.takeScreenShot("Product in cart with reduced price");

        homePage.closePage();
    }

}
