import homepage.FilterPage;
import homepage.HomePage;
import homepage.SortingPage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import java.util.List;

@Epic("Filter Test")
@Test(description = "Filter tests")
public class FilterTest {

    HomePage homePage;
    SortingPage sortingPage;
    FilterPage filterPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        sortingPage = new SortingPage();
        filterPage = new FilterPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Filter products by price: 0.00 - 99.99")
    public void filter_products_by_price_0_99() {
        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        int count = filterPage.getCount(filterPage.countBelow100Selector);

        //select filter 0.00 LEU - 99.99 LEU
        filterPage.clickFilterPrice(filterPage.filterPriceBelow100);

        List<Double> prices = sortingPage.getPriceList();

        for (Double price : prices) {
            Assert.assertTrue(price <= 99.99 && price >= 0.00);
        }

        Assert.assertEquals(prices.size(), count);

        screenShooter.takeScreenShot("Filter by price 0-99");

        homePage.closePage();
    }

    @Test(description = "Filter products by price: 100.00 and above")
    public void filter_products_by_price_100_and_above() {
        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        int count = filterPage.getCount(filterPage.countAbove100Selector);

        //select filter 100 LEU and above
        filterPage.clickFilterPrice(filterPage.filterPriceAbove100);

        List<Double> prices = sortingPage.getPriceList();

        for (Double price : prices) {
            Assert.assertTrue(price >= 100.00);
        }

        Assert.assertEquals(prices.size(), count);

        screenShooter.takeScreenShot("Filter by price > 100");

        homePage.closePage();
    }

    @Test(description = "Filter products after color")
    public void filter_products_by_color() {
        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        String colorFilter = filterPage.getColorFromFilter(0);

        filterPage.selectColor(0);

        //chek after color name
        filterPage.checkFilteredColor(colorFilter, filterPage.product);

        screenShooter.takeScreenShot("Filter by color");

        homePage.closePage();
    }

    @Test(description = "Filter products by size")
    public void filter_products_by_size() {
        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        int nmbOfItem = filterPage.getCount(filterPage.filterBySizeCount.get(2));

        //2 = witch button is pushed
        filterPage.selectSize(2);

        int nmbItemsFiltered = filterPage.getNmbItemsFiltered(filterPage.nmbItem);

        Assert.assertEquals(nmbOfItem, nmbItemsFiltered);

        screenShooter.takeScreenShot("Filter by size");

        homePage.closePage();
    }
}
