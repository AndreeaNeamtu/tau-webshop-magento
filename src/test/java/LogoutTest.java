import com.codeborne.selenide.Condition;
import homepage.HomePage;
import homepage.LoginPage;
import homepage.LogoutPage;
import io.qameta.allure.Epic;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Epic("Logout Test")
@Test(description = "Logout test")
public class LogoutTest {
    HomePage homePage;
    LoginPage loginPage;
    LogoutPage logoutPage;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        loginPage = new LoginPage();
        logoutPage = new LogoutPage();
    }

    @Test(description = "Lougout with success")
    public void logout_user() {
        homePage.openPage();

        homePage.clickOnMenuButton("Log In");
        loginPage.fillInEmailAddress("vasile@dom.com");
        loginPage.fillInPassword("abc123");
        loginPage.clickOnLoginButton();

        String welcomeMessage = "Hello, Vasile Ion!";
        loginPage.verifyWelcomeMessage(welcomeMessage);

        //logout the user
        homePage.clickOnMenuButton("Log Out");

        logoutPage.verifyLogoutMessage("You are now logged out",
                "You have logged out and will be redirected to our homepage in 5 seconds.");

        //wait 5 seconds to be displayed the main page
        homePage.slideshow.waitUntil(Condition.visible, 5000);

        homePage.closePage();
    }
}
