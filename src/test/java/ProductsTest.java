import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import homepage.CartPage;
import homepage.HomePage;
import homepage.LoginPage;
import homepage.ProductPage;
import io.qameta.allure.Epic;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import setup.ScreenShooter;

import static com.codeborne.selenide.Selenide.$;

@Epic("Product Tests")
@Test(description = "Product tests")
public class ProductsTest {

    HomePage homePage;
    ProductPage productPage;
    CartPage cartPage;
    LoginPage loginPage;
    ScreenShooter screenShooter;

    @BeforeClass
    public void setup() {
        homePage = new HomePage();
        productPage = new ProductPage();
        cartPage = new CartPage();
        loginPage = new LoginPage();
        screenShooter = new ScreenShooter();
    }

    @Test(description = "Add one product directly to cart")
    public void add_products_directly_in_basket() {

        homePage.openPage();

        homePage.goToCategory(homePage.jewelryCategory, 2);

        screenShooter.takeScreenShot("Before adding the product to basket");

        String newProductName = productPage.getProductName(1);

        productPage.addToCart(1);

        String message = " was added to your shopping cart.";

        Assert.assertEquals(cartPage.messageAddToCartSuccess.getText(), newProductName + message);

        screenShooter.takeScreenShot("Add to cart directly");

        homePage.closePage();
    }

    @Test(description = "Add product to cart without selecting color and size")
    public void add_product_without_select_size_and_color() {

        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        int nmbOfItem = homePage.generateRandomNumberBetween(0, 4);

        productPage.selectItem(nmbOfItem);

        productPage.addToCart();

        String messageRequiredField = "This is a required field.";

        for (SelenideElement selenideElement : productPage.requiredField) {
            selenideElement.shouldHave(Condition.text(messageRequiredField));
        }

        screenShooter.takeScreenShot("Required fields: size and color");

        homePage.closePage();
    }

    @Test(description = "Add 1 product to cart, selecting color and size")
    public void add_product_to_cart() {

        homePage.openPage();

        homePage.goToCategory(homePage.pantsCategory, 1);

        int nmbOfItem = homePage.generateRandomNumberBetween(0, 3);

        productPage.selectItem(nmbOfItem);

        String productName = productPage.getProductName();

        productPage.selectColor();

        productPage.selectSize();

        productPage.addToCart();

        String message = " was added to your shopping cart.";

        productPage.messageSuccess.shouldBe(Condition.visible);

        productPage.messageSuccess.shouldHave(Condition.text(productName + message));

        screenShooter.takeScreenShot("Product in cart");

        homePage.closePage();
    }

    @Test(description = "Change quantity")
    public void change_quantity_of_product_and_add_to_cart() {

        homePage.openPage();

        homePage.goToCategory(homePage.jewelryCategory, 2);

        productPage.selectItem(3);

        String mnbOfItemsAdded = productPage.addQuantity("4");

        screenShooter.takeScreenShot("Change quantity");

        productPage.addToCart();

        String nmbOfItemInCart = cartPage.getNumberOfItemsFromCart();

        Assert.assertEquals(mnbOfItemsAdded, nmbOfItemInCart);

        homePage.closePage();
    }

    @Test(description = "Check if product is in stock")
    public void check_if_product_is_in_stock() {

        homePage.openPage();

        homePage.goToCategory(homePage.jewelryCategory, 2);
        productPage.selectItem(0);

        String productName = productPage.getProductName();

        productPage.addQuantity("5");

        productPage.addToCart();

        productPage.errorMsg.shouldHave(Condition.text("The requested quantity for \"" +
                productName + "\" is not available."));

        screenShooter.takeScreenShot("Not in stock");

        homePage.closePage();
    }

    @Test(description = "Add product to wishlist")
    public void add_product_to_wishlist() {
        homePage.openPage();

        homePage.clickOnMenuButton("Log In");
        loginPage.fillInEmailAddress("vasile@dom.com");
        loginPage.fillInPassword("abc123");
        loginPage.clickOnLoginButton();

        String welcomeMessage = "Hello, Vasile Ion!";
        loginPage.verifyWelcomeMessage(welcomeMessage);

        homePage.goToCategory(homePage.pantsCategory, 1);

        productPage.selectItem(2);

        String productName = productPage.getProductName();

        productPage.selectColor();

        productPage.selectSize();

        productPage.clickAddToWishlist();

        String message = " has been added to your wishlist.";

        SelenideElement messageWishlist = $(".success-msg span");
        messageWishlist.shouldHave(Condition.text(productName + message));

        screenShooter.takeScreenShot("Add to wishlist");

        homePage.closePage();
    }

    @Test(description = "Add review for one product")
    public void add_review() {
        homePage.openPage();

        homePage.goToCategory(homePage.vipCategory, 10);
        productPage.selectItem(3);

        productPage.clickOnReviews();

        productPage.addReview.click();

        productPage.reviewForm.shouldBe(Condition.visible);

        productPage.rateQuality();

        productPage.rateDelivery();

        productPage.ratePrice();

        productPage.rateDesign();

        productPage.rateValue();

        productPage.writeReview("very nice");
        productPage.writeSummary("good");
        productPage.writeNickName("John");

        productPage.clickOnSubmit();

        productPage.message.shouldBe(Condition.visible);
        productPage.message.shouldHave(Condition.exactText("Your review has been accepted for moderation."));

        screenShooter.takeScreenShot("Review");

        homePage.closePage();
    }
}

